var fs = require('fs')

fs.readFile( 'samplefile.txt', 'utf8',function(err, data){
    if(err){
        console.log(err)
    }else{
        console.log(data)
    }
})

// fs.writeFile('newfile.js', 'console.log("Hello World")', function(err){
//     if(err) console.log(err)
// })

fs.unlink('newfile.js', function(){})


function sub(num1, num2, myCallback) {
       let sum = num1 - num2;

        myCallback(sum);
    }
      

sub(4, 2, function(data){
    console.log(data)
})