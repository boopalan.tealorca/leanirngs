const express = require('express')


const app = express();


app.get('/get', function(req, res){
    res.send("Hello world")
})


app.get('/get/:id', function(req, res){
    const id = req.params.id
    res.send("Hello world : " + id)
})


app.get('/getq', function(req, res){
    const id = req.query.id
    res.send("Hello world : " + id)
})

app.post("/post", function(req, res){
    let json = req.body
})

app.listen(8080, function(req, res) {
    console.log("running")
})